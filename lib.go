/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package main

import (
    "io/ioutil"
    "os"
    "os/exec"
    "regexp"
    "sort"
    "strconv"
    "strings"
    "time"
    "unicode"
)

const clipTime = 900
const minLength = 16
const defaultLength = 32
const defaultCharset = "[:graph:]"

type passwordMeta struct {
    path string
    name string
    mtime time.Time
}

// implement sort.interface for password lists based on mtime
type ByMtime []passwordMeta
func (a ByMtime) Len() int           { return len(a) }
func (a ByMtime) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByMtime) Less(i, j int) bool { return a[i].mtime.Before(a[j].mtime) }


type passwordData struct {
    value string
    length int
    charset string
}


func args2prefixes() []string {
    prefixes := []string{""}
    if (len(os.Args) > 1) {
        prefixes = os.Args[1:]
    }

    return prefixes
}

func getPassDir() string {
    passDir := os.Getenv("PASSWORD_STORE_DIR")
    if passDir == "" {
        passDir = os.Getenv("HOME") + "/.password-store"
    }

    return passDir
}

func listPasswords(dir string) []passwordMeta {
    passwords := []passwordMeta{}
    dirPath := getPassDir() + "/" + dir
    if dir != "" {
        dir += "/"
    }

    files, err := ioutil.ReadDir(dirPath)
    if err != nil {
        goto Ret
    }

    for _, file := range files {
        name := dir + file.Name()

        // ignore hidden files
        if (file.Name())[0] == '.' {
            continue
        }

        // ignore symlinks
        if file.Mode()&os.ModeSymlink != 0 {
            continue
        }

        // ignore files that can't be read
        fh, err := os.OpenFile(getPassDir() + "/" + name, os.O_RDONLY, 0666)
        if err != nil {
            continue
        }
        fh.Close()

        if !file.Mode().IsDir() && name[len(name) - 4:] == ".gpg" {
            passwords = append(passwords, passwordMeta{
                path: dirPath + "/" + file.Name(),
                name: name[0:len(name)-4],
                mtime: file.ModTime(),
            })
        } else if file.Mode().IsDir() {
            passwords = append(passwords, listPasswords(name)...)
        }
    }

    Ret:
    return passwords
}

func getOldestPassword(prefixes []string) (passMeta passwordMeta, ok bool) {
    passwords := []passwordMeta{}
    for _, prefix := range(prefixes) {
        passwords = append(passwords, listPasswords(prefix)...)
    }
    sort.Sort(ByMtime(passwords))
    if len(passwords) < 1 {
        return passwordMeta{}, false
    }

    return passwords[0], true
}

func guessLength(password string) int {
    l := len(password)

    if l < minLength {
        return defaultLength
    }

    return l
}

func guessCharset(password string) string {
    if len(password) < minLength {
        return defaultCharset
    }

    haveSpace := false
    haveGraph := false
    for _, c := range(password) {
        if unicode.IsSpace(c) {
            haveSpace = true
        }
        if !(unicode.IsLetter(c) || unicode.IsDigit(c)) {
            haveGraph = true
        }
    }
    if haveSpace {
        return "[:print:]"
    }
    if haveGraph {
        return "[:graph:]"
    }

    return "[:alnum:]"
}

func readPassword(passwordName string) (passwordData, error) {
    passContent, err := exec.Command("pass", "show", passwordName).Output()
    if err != nil {
        return passwordData{}, err
    }
    lines := strings.Split(string(passContent), "\n")
    passData := passwordData{
        value: string(passContent),
        length: defaultLength,
        charset: defaultCharset,
    }

    // use given configuration if any, otherwise guess it
    var matches []string = nil
    if len(lines) > 1 {
        confRe := regexp.MustCompile(`^#\s*rotpass\s*:\s*(?:(\d+)$|(\d+)\s+(.+)$|(.+)$)`)
        matches = confRe.FindStringSubmatch(lines[len(lines) - 1])
    }
    if matches != nil {
        if (matches[1] != "") {
            if passData.length, err = strconv.Atoi(matches[1]); err != nil {
                return passwordData{}, err
            }
        } else if (matches[4] != "") {
            passData.charset = matches[4]
        } else {
            if passData.length, err = strconv.Atoi(matches[2]); err != nil {
                return passwordData{}, err
            }
            passData.charset = matches[3]
        }
    } else {
        passData.length = guessLength(lines[0])
        passData.charset = guessCharset(lines[0])
    }

    return passData, nil
}
