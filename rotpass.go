/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package main

import (
    "fmt"
    "log"
    "os"
    "os/exec"
    "strconv"
    "strings"
)

func execUpdate(passName string, passData passwordData) {
    args := []string{
        "update",
        "-c",
        passName,
    }
    envVars := map[string]string{
        "PASSWORD_STORE_CLIP_TIME": strconv.Itoa(clipTime),
        "PASSWORD_STORE_CHARACTER_SET": passData.charset,
        "PASSWORD_STORE_GENERATED_LENGTH": strconv.Itoa(passData.length),
    }
    for k, v := range(envVars) {
        os.Setenv(k, v)
        log.Println(k + "=" + v)
    }
    fmt.Println("pass " + strings.Join(args, " "))
    cmd := exec.Command("pass", args...)
    cmd.Stdin = os.Stdin
    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr
    cmd.Env = os.Environ()
    if err := cmd.Run(); err != nil {
        log.Fatal(err)
    }
}

func main() {
    prefixes := args2prefixes()
    if passMeta, ok := getOldestPassword(prefixes); ok {
        passData, err := readPassword(passMeta.name)
        if err != nil {
            log.Fatalf("Failed to read password %s: %s\n", passMeta.name, err)
        }
        execUpdate(passMeta.name, passData)
    }
}
