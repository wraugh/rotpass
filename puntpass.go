/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package main

import (
    "log"
    "os"
    "time"
)

func main() {
    prefixes := args2prefixes()
    if passMeta, ok := getOldestPassword(prefixes); ok {
        now := time.Now()
        if err := os.Chtimes(passMeta.path, now, now); err != nil {
            log.Fatal(err)
        }
    }
}
