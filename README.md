# rotpass - update your oldest password

rotpass calls [pass update][1] on the oldest password in your [password
store][2].

[1]: https://github.com/roddhjav/pass-update
[2]: https://www.passwordstore.org/

## Contributing

This package can be improved in many ways. For example,

 - respect the `PASSWORD_STORE_CLIP_TIME` environment variable if set
 - more sophisticated password age criterion, e.g. based on the latest git
   commit where a password is the only changed file
 - add a help / usage message
 - document how password generation parameters can be configured
